#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import urllib2
from suds.client import Client
from suds import WebFault

__author__ = u"Fabian Wurster (d3473@gmail.com)"
__copyright__ = u"Copyright (C) 2016 Fabian Wurster"
__version__ = u"1.0"

PROGRAM_PATH = os.path.dirname(os.path.realpath(__file__)) + '\\'

ilias_soap = u"https://ilias.hs-heilbronn.de/webservice/soap/server.php?wsdl"
ilias_client_id = u"hshn"

# Insert your Credentials here #
username = u""
password = u""
################################

conf = "file:/" + PROGRAM_PATH.replace('\\', '/') + "temp.xml"
response = urllib2.urlopen(ilias_soap)
html = response.read().replace('"https://ilias.hs-heilbronn.de:443/webservice/soap/server.php"',
                               '"http://ilias.hs-heilbronn.de/webservice/soap/server.php"')
with open('temp.xml', 'w') as f:
    f.write(html)
client = Client(conf)
os.remove(PROGRAM_PATH + 'temp.xml')
print client
try:
    result = client.service.login(ilias_client_id, username, password)
    print u"Access Token?: %s" % result
except WebFault:
    print u"Failed to login!\nAre the username and password correct?"
